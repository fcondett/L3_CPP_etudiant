#include <iostream>
#include "fibonacci.hpp"
int main()
{
	std::cout<<"recursif: "<<fibonacciRecursif(7)<<std::endl;
	std::cout<<"iteratif: "<<fibonacciIteratif(7)<<std::endl;
	return 0;
}
