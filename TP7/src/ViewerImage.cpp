#include <gtkmm.h>
#include "ViewerImage.hpp"
#include "Image.hpp"
ViewerImage::ViewerImage(int argc, char** argv): _kit(argc,argv)
{
    _window.set_title("tp7");
    _window.set_default_size(640,480);
    Image im(100,100);
    remplir(im);
    Image imBord=bordure(im, 0, 10);
    ecrirePnm(imBord, "bord2.pnm");
    _img.set("bord2.pnm");
}

void ViewerImage::run()
{
    _window.add(_img);
    _window.show_all();
    _kit.run(_window); // lance la boucle evenementielle return 0;
}
