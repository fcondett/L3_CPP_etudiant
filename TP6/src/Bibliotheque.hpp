#ifndef BIBLIOTHEQUE_HPP
#define BIBLIOTHEQUE_HPP
#include <vector>
#include "Livre.hpp"
class Bibliotheque: public std::vector<Livre>
{
public:
    void afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void ecrireFichier(const std::string& nomFichier) const;
    void lireFichier(const std::string& nomFichier);
};
#endif
