#ifndef DOUBLER_HPP_
#define DOUBLER_HPP_

struct Couleur {
    double _r, _g, _b;

    Couleur()
    {
        _r=0;
        _g=0;
        _b=0;
    }

    Couleur(double r, double g, double b)
    {
        _r=r;
        _g=g;
        _b=b;
    }
};

#endif
