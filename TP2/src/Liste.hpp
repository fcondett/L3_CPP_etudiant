#ifndef LISTE_HPP_
#define LISTE_HPP_
struct Noeud {
    int _valeur;
    Noeud* _suivant;
};

struct Liste {
    Noeud* _tete;
    Liste();
    ~Liste();
    void ajouterDevant(int val);
    int getTaille() const;
    int getElement(int indice) const;
};
#endif
