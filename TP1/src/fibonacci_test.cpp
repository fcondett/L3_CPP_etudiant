#include "fibonacci.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, Test_fibo_recursif)
{
    int result[]={0,1,1,2,3};
    for(int i=0; i<5; i++)
    {
        CHECK_EQUAL(fibonacciRecursif(i), result[i]);
    }
}

TEST(GroupFibo, Test_fibo_iteratif)
{
    int result[]={0,1,1,2,3};
    for(int i=0; i<5; i++)
    {
        CHECK_EQUAL(fibonacciIteratif(i), result[i]);
    }
}
