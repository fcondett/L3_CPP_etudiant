#include <cmath>
#include <iostream>
#include <string>
#include "PolygoneRegulier.hpp"



PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):FigureGeometrique(couleur)
{
    //_points = new Point[nbCotes];
    Point p;
    _nbPoints=nbCotes;
    for(int i=0;i<nbCotes;i++)
    {
        p._x = rayon * cos(2*M_PI*i/nbCotes) + centre._x;
        p._y = rayon * sin(2*M_PI*i/nbCotes) + centre._y;
        _points.push_back(p);
    }
}

PolygoneRegulier::~PolygoneRegulier()
{
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
    double sr=_couleur._r;
    double sg=_couleur._g;
    double sb=_couleur._b;

    std::cout<<"PolygoneRegulier "<<sr<<"_"<<sg<<"_"<<sb<<" ";
    for(int i=0;i<_nbPoints;i++)
    {
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
    }
    std::cout<<std::endl;

    context->set_source_rgb(sr,sg,sb);
    context->set_line_width(10.0);
    context->move_to(getPoint(0)._x, getPoint(0)._y);
    for(int i=1;i<_nbPoints;i++)
    {
        context->line_to(getPoint(i)._x, getPoint(i)._y);
    }
    context->line_to(getPoint(0)._x, getPoint(0)._y);
    context->stroke();
}

int PolygoneRegulier::getNbPoints() const
{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const
{
    return _points[indice];
}
