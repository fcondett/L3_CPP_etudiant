#include "Inventaire.hpp"
#include "Bouteille.hpp"

std::ostream& operator <<(std::ostream& os, const Inventaire& i) {

    for(Bouteille bouteille : i._bouteilles)
        os<<bouteille;
    return os;
}

