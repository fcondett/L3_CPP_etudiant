#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"
#include <iostream>

int main() {
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("tata");
    m.ajouterClient("titi");
    m.supprimerClient(1);
    m.afficherClients();
    return 0;
}

