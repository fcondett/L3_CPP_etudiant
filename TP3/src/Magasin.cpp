#include <vector>
#include <algorithm>
#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"
#include "Magasin.hpp"

Magasin::Magasin() {
    _idCourantClient=0;
    _idCourantProduit=0;
}

int Magasin::nbClients() const
{
    return _clients.size();
}

void Magasin::ajouterClient(const std::string& nom)
{
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

void Magasin::afficherClients() const
{
    for(int i=0; i<nbClients(); i++)
    {
        _clients[i].afficherClient();
    }
}

void Magasin::supprimerClient(int id)
{
    for(int i=0; i<nbClients(); i++)
    {
        if(_clients[i].getId() == id)
        {
            while(i<nbClients()-1)
            {
                int j=i+1;
                std::swap(_clients[i], _clients[j]);
                i++;
            }
            _clients.pop_back();
            return;
        }
    }
    throw std::string("erreur: le client n'existe pas");
}


int Magasin::nbProduits() const
{
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string& nom)
{
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit++;
}

void Magasin::afficherProduits() const
{
    for(int i=0; i<nbProduits(); i++)
    {
        _produits[i].afficherProduit();
    }
}

void Magasin::supprimerProduit(int id)
{
    for(int i=0; i<nbProduits(); i++)
    {
        if(_produits[i].getId() == id)
        {
            while(i<nbProduits()-1)
            {
                int j=i+1;
                std::swap(_produits[i], _produits[j]);
                i++;
            }
            _produits.pop_back();
            return;
        }
    }
    throw std::string("erreur: le produit n'existe pas");
}

int Magasin::nbLocations() const
{
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit)
{
    for(unsigned int i=0;i<_locations.size();i++)
    {
        if(_locations[i]._idClient == idClient && _locations[i]._idProduit == idProduit)
            throw std::string("erreur: la location existe déjà");
    }
    _locations.push_back(Location(idClient, idProduit));
}

void Magasin::afficherLocations() const
{
    for(int i=0; i<nbLocations(); i++)
    {
        _locations[i].afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit)
{
    for(int i=0; i<nbLocations(); i++)
    {
        if(_locations[i]._idClient == idClient && _locations[i]._idProduit == idProduit)
        {
            while(i<nbLocations()-1)
            {
                int j=i+1;
                std::swap(_locations[i], _locations[j]);
                i++;
            }
            _locations.pop_back();
            return;
        }
    }
    throw std::string("erreur: la location n'existe pas");
}

bool Magasin::trouverClientDansLocation(int idClient) const
{
    for(int i=0; i<nbLocations(); i++)
    {
        if(_locations[i]._idClient == idClient)
            return true;
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const
{
    bool nonlibre;
    std::vector<int> libres;

    for(int i=0;i<nbClients();i++)
    {
        nonlibre=false;
        for(int j=0;j<nbLocations();j++)
        {
            if(_locations[j]._idClient == _clients[i].getId())
            {
                nonlibre=true;
                break;
            }
        }
        if(!nonlibre)
            libres.push_back(_clients[i].getId());
    }
    return libres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const
{
    for(int i=0; i<nbLocations(); i++)
    {
        if(_locations[i]._idProduit == idProduit)
            return true;
    }
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const
{
    bool nonlibre;
    std::vector<int> libres;

    for(int i=0;i<nbProduits();i++)
    {
        nonlibre=false;
        for(int j=0;j<nbLocations();j++)
        {
            if(_locations[j]._idProduit == _produits[i].getId())
            {
                nonlibre=true;
                break;
            }
        }
        if(!nonlibre)
            libres.push_back(_produits[i].getId());
    }
    return libres;
}
