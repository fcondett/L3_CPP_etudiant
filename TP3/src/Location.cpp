#include <iostream>
#include <string>
#include "Location.hpp"

Location::Location(int idClient, int idProduit)
{
    _idClient=idClient;
    _idProduit=idProduit;
}

void Location::afficherLocation() const
{
    std::cout<<"Location(" + std::to_string(_idClient) + "," + std::to_string(_idProduit) + ")"<<std::endl;
}
