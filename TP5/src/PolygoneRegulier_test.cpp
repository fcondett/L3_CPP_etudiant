#include "PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolyReg) { };

TEST(GroupPolyReg, PolyReg_constr)  {
    PolygoneRegulier penta(Couleur(0,1,0), Point(100,200), 50, 5);
    CHECK(penta.getNbPoints()==5);
    CHECK(penta.getPoint(1)._x==115);
}
