#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_constr)  {
    Produit c(42,"toto");
    CHECK(c.getId() == 42);
    CHECK(c.getDescription() == "toto");
}
