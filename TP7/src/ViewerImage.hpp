#ifndef VIEWERIMAGE_HPP
#define VIEWERIMAGE_HPP
#include <gtkmm.h>
#include "Image.hpp"
class ViewerImage {
    private:
    Gtk::Main _kit;
    Gtk::Window _window;
    Gtk::Image _img;

    public:
    ViewerImage(int argc, char** argv);
    void run();
};
#endif
