#include "Doubler.hpp"
#include "Liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_constr)  {
    Liste l;
    CHECK(l._tete == nullptr);
}

TEST(GroupListe, Liste_ajdevant)  {
    Liste l;
    l.ajouterDevant(42);
    CHECK(l._tete->_valeur == 42);
    CHECK(l._tete->_suivant == nullptr);
}

TEST(GroupListe, Liste_taille)  {
    Liste l;
    CHECK(l.getTaille() == 0);
    l.ajouterDevant(5);
    CHECK(l.getTaille() == 1);
    l.ajouterDevant(6);
    CHECK(l.getTaille() == 2);
}

TEST(GroupListe, Liste_getelem)  {
    Liste l;
    CHECK(l.getElement(1) == -1);
    l.ajouterDevant(42);
    l.ajouterDevant(15);
    l.ajouterDevant(30);
    CHECK(l.getElement(0) == 30);
    CHECK(l.getElement(1) == 15);
    CHECK(l.getElement(2) == 42);
}
