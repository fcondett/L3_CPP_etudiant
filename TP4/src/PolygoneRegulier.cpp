#include <cmath>
#include <iostream>
#include <string>
#include "PolygoneRegulier.hpp"



PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):FigureGeometrique(couleur)
{
    //_points = new Point[nbCotes];
    Point p;
    _nbPoints=nbCotes;
    for(int i=0;i<nbCotes;i++)
    {
        p._x = rayon * cos(2*M_PI*i/nbCotes) + centre._x;
        p._y = rayon * sin(2*M_PI*i/nbCotes) + centre._y;
        _points.push_back(p);
    }
}

PolygoneRegulier::~PolygoneRegulier()
{
}

void PolygoneRegulier::afficher() const
{
    double sr=_couleur._r;
    double sg=_couleur._g;
    double sb=_couleur._b;

    std::cout<<"PolygoneRegulier "<<sr<<"_"<<sg<<"_"<<sb<<" ";
    for(int i=0;i<_nbPoints;i++)
    {
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
    }
    std::cout<<std::endl;
}

int PolygoneRegulier::getNbPoints() const
{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const
{
    return _points[indice];
}
