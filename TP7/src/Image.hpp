#ifndef IMAGE_HPP_
#define IMAGE_HPP_
#include <string>

class Image {
private:
    int _largeur;
    int _hauteur;
    int* _pixels;

public:
    Image(int largeur, int hauteur);
    ~Image();
    Image(const Image & img);
    int getLargeur() const;
    int getHauteur() const;
    void setPixel(int i, int j, int couleur);
    const int& getPixel(int i, int j) const;
    const Image & operator=(const Image & img);
};

void ecrirePnm(const Image & img, const std::string & nomFichier);
void remplir(Image & img);
Image bordure(const Image & img, int couleur, int epaisseur);
#endif
