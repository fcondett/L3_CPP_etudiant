#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_
#include "Couleur.hpp"

class FigureGeometrique {
public:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur & couleur);
    virtual ~FigureGeometrique() {}
    const Couleur & getCouleur() const;
    virtual void afficher() const =0;
};

#endif
