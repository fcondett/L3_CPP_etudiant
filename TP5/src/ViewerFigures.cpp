#include <gtkmm.h>
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"
ViewerFigures::ViewerFigures(int argc, char** argv): _kit(argc,argv)
{
    _window.set_title("figures");
    _window.set_default_size(640,480);
}

void ViewerFigures::run()
{
    _window.add(dessin);
	_window.show_all();
    _kit.run(_window); // lance la boucle evenementielle return 0;
}
