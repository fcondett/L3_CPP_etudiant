#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image_test1)  {
    Image i(2,2);
    i.setPixel(0,0,50);
    int x = i.getPixel(0,0);
    CHECK_EQUAL(x, 50);
    try {
        i.getPixel(4,4);
        FAIL( "exception non levée");
    }
    catch(const std::string& str)
    {
        CHECK_EQUAL(str, "erreur : coordonnés invalides");
    }

    try {
        i.setPixel(4,4,2);
        FAIL( "exception non levée");
    }
    catch(const std::string& str)
    {
        CHECK_EQUAL(str, "erreur : coordonnés invalides");
    }
}



