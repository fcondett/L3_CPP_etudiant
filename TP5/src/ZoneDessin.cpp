#include <gtkmm.h>
#include <random>
#include "ZoneDessin.hpp"
#include "Point.hpp"
#include "Couleur.hpp"
#include "PolygoneRegulier.hpp"
#include "Ligne.hpp"
using namespace std;
ZoneDessin::ZoneDessin()
{
    _figures.push_back(std::make_unique<Ligne>(Couleur(0,0,0), Point(10,10), Point(60,60)));
    _figures.push_back(std::make_unique<PolygoneRegulier>(Couleur(0.5,0.5,0.5), Point(150,150), 50, 6));
    add_events(Gdk::BUTTON_PRESS_MASK);
    signal_button_press_event().connect( sigc::mem_fun(*this, &ZoneDessin::gererClic));
}
/*
ZoneDessin::~ZoneDessin()
{
    for (FigureGeometrique * ptrFig : _figures)
            delete ptrFig;
}
*/
bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context)
{
    for (const auto & ptrFig : _figures)
            ptrFig->afficher(context);
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton *event)
{

    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1) )
    {
        default_random_engine re(time(0));
        uniform_int_distribution<int> distribFace{2, 6};
        uniform_int_distribution<int> distribRay{5, 150};
        int rFace=distribFace(re);
        int rRay=distribRay(re);
        _figures.push_back(std::make_unique<PolygoneRegulier>(Couleur(0.8,0.2,0.2),
                           Point(event->x, event->y), rRay, rFace));
        queue_draw();
    }
    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) )
    {
        if(!_figures.empty())
            _figures.pop_back();
        queue_draw();
    }

    return true;
}
