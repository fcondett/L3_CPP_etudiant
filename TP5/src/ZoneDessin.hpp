#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP
#include <gtkmm.h>
#include "FigureGeometrique.hpp"
class ZoneDessin : public Gtk::DrawingArea{
private:
    std::vector<std::unique_ptr<FigureGeometrique>> _figures;
public:
    ZoneDessin();
   // ~ZoneDessin();
protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;

    bool gererClic(GdkEventButton* event);
};
#endif
