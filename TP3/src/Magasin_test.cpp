#include <vector>
#include "Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_client)  {
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("tata");
    m.ajouterClient("titi");
    CHECK(m.nbClients() == 3);
    m.supprimerClient(0);
    CHECK(m.nbClients() == 2);
    CHECK_THROWS(std::string, m.supprimerClient(5));
}

TEST(GroupMagasin, Magasin_produit)  {
    Magasin m;
    m.ajouterProduit("toto");
    m.ajouterProduit("tata");
    m.ajouterProduit("titi");
    CHECK(m.nbProduits() == 3);
    m.supprimerProduit(0);
    CHECK(m.nbProduits() == 2);
    CHECK_THROWS(std::string, m.supprimerProduit(10));
}

TEST(GroupMagasin, Magasin_location)  {
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("tata");
    m.ajouterClient("titi");
    m.ajouterProduit("p1");
    m.ajouterProduit("p2");
    m.ajouterLocation(0,0);
    CHECK(m.nbLocations() == 1);
    CHECK_THROWS(std::string, m.ajouterLocation(0,0));
    CHECK_THROWS(std::string, m.supprimerLocation(2,0));

    std::vector<int> clLibres=m.calculerClientsLibres();
    CHECK(clLibres.size() == 2);
    CHECK(clLibres[0] == 1);
    std::vector<int> prLibres=m.calculerProduitsLibres();
    CHECK(prLibres.size() == 1);
    CHECK(prLibres[0] == 1);
}
