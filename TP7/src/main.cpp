#include "Image.hpp"
#include "ViewerImage.hpp"
#include <iostream>

int main(int argc, char** argv) {
    ViewerImage viewer(argc, argv);
    viewer.run();
    return 0;
}

