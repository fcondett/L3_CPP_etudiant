#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_constr)  {
    Ligne l(Couleur(42,41,45), Point(1,1), Point(2,2));
    CHECK(l.getCouleur()._r==42);
    CHECK(l.getP0()._x==1);
    CHECK(l.getP1()._y==2);
}


