#include <iostream>
#include <math.h>
struct Vecteur3D {
    float x;
    float y;
    float z;
};

void afficher(Vecteur3D v)
{
    std::cout<<"( "<<v.x<<", "<<v.y<<", "<<v.z<<" )"<<std::endl;
}

float norme(Vecteur3D v)
{
    return sqrt(pow(v.x,2) + pow(v.y,2) + pow(v.z,2));
}

float produitScalaire(Vecteur3D v1, Vecteur3D v2)
{
    return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

Vecteur3D addition(Vecteur3D v1, Vecteur3D v2)
{
    Vecteur3D ret={v1.x+v2.x, v1.y+v2.y, v1.z+v2.z};
    return ret;
}
