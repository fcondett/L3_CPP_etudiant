struct Vecteur3D {
    float x;
    float y;
    float z;
};

void afficher(Vecteur3D v);

float norme(Vecteur3D v);

float produitScalaire(Vecteur3D v1, Vecteur3D v2);

Vecteur3D addition(Vecteur3D v1, Vecteur3D v2);
