#include "Vecteur3.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, Test_vect3_norme)
{
    CHECK_EQUAL(norme({2,3,6}), 7);
}

TEST(GroupVecteur3, Test_vect3_produitscal)
{
    Vecteur3D v{2,3,6};
    CHECK_EQUAL(produitScalaire(v,v), 49);
}

TEST(GroupVecteur3, Test_vect3_addition)
{
    Vecteur3D v{2,3,6};
    Vecteur3D resultat{4,6,12};
    CHECK_EQUAL(addition(v,v).x, resultat.x);
    CHECK_EQUAL(addition(v,v).y, resultat.y);
    CHECK_EQUAL(addition(v,v).z, resultat.z);
}
