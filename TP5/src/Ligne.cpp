#include <iostream>
#include <string>
#include <gtkmm.h>
#include "Ligne.hpp"
#include "FigureGeometrique.hpp"
#include "Couleur.hpp"
#include "Point.hpp"

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1):FigureGeometrique(couleur)
{
    _p0=p0;
    _p1=p1;
}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
    double sr=_couleur._r;
    double sg=_couleur._g;
    double sb=_couleur._b;
    int sp0x=_p0._x;
    int sp0y=_p0._y;
    int sp1x=_p1._x;
    int sp1y=_p1._y;
    std::cout<<"Ligne "<<sr<<"_"<<sg<<"_"<<sb<<" "<<sp0x<<"_"<<sp0y<<" "<<sp1x<<"_"<<sp1y<<std::endl;

    context->set_source_rgb(sr,sg,sb);
    context->set_line_width(10.0);
    context->move_to(sp0x, sp0y);
    context->line_to(sp1x, sp1y);
    context->stroke();
}
const Point & Ligne::getP0() const
{
    return _p0;
}
const Point & Ligne::getP1() const
{
    return _p1;
}

