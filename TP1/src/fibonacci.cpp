int fibonacciRecursif(int nbterme)
{
	if (nbterme < 2)
		return nbterme;
	else
		return fibonacciRecursif(nbterme-1) + fibonacciRecursif(nbterme-2);
}

int fibonacciIteratif(int nbterme)
{
	int u = 0;
	int v = 1;
	int i, t;

    if (nbterme < 2)
        return nbterme;
    else
    {
        for(i = 2; i <= nbterme; i++) {
            t = u + v;
            u = v;
            v = t;
        }
        return v;
    }
}
