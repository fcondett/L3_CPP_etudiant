#include "Liste.hpp"
Liste::Liste()
{
    _tete=nullptr;
}

Liste::~Liste()
{
    while(_tete)
    {
        Noeud * prec=_tete;
        _tete=_tete->_suivant;
        delete prec;
        if(getTaille()==0)
        {
            delete _tete;
        }
    }
}

void Liste::ajouterDevant(int val)
{
    _tete= new Noeud {val, _tete};
}

int Liste::getTaille() const
{
    int taille=0;
    Noeud* temp=_tete;
    while(temp)
    {
        temp = temp->_suivant;
        taille++;
    }
    return taille;
}

int Liste::getElement(int indice) const
{
    Noeud* temp=_tete;
    for(int i=0;i<getTaille();i++)
    {
        if(i==indice)
            return temp->_valeur;
        temp=temp->_suivant;
    }
    return -1;
}
