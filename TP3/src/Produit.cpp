#include <iostream>
#include <string>
#include "Produit.hpp"

Produit::Produit(int id, const std::string& description)
{
    _id=id;
    _description=description;
}

void Produit::afficherProduit() const
{
    std::cout<<"Produit(" + std::to_string(_id) + "," + _description + ")"<<std::endl;
}

int Produit::getId() const
{
    return _id;
}

const std::string& Produit::getDescription() const
{
    return _description;
}
