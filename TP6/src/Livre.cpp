#include "Livre.hpp"
#include <string>
#include <sstream>
using namespace std;
Livre::Livre(): _titre(" "), _auteur(" "), _annee(0)
{}

Livre::Livre(const string & titre, const string & auteur, int annee)
{
    if(titre.find(";") != string::npos)
        throw string("erreur : titre non valide (';' non autorisé)");

    if(titre.find("\n") != string::npos)
        throw string("erreur : titre non valide ('\n' non autorisé)");

    if(auteur.find(";") != string::npos)
        throw string("erreur : auteur non valide (';' non autorisé)");

    if(auteur.find("\n") != string::npos)
        throw string("erreur : auteur non valide ('\n' non autorisé)");

    _titre=titre;
    _auteur=auteur;
    _annee=annee;
}

const string& Livre::getTitre() const
{
    return _titre;
}

const string& Livre::getAuteur() const
{
    return _auteur;
}

int Livre::getAnnee()
{
    return _annee;
}

bool Livre::operator<(const Livre& livre2) const
{
    if(_auteur != livre2.getAuteur())
        return _auteur < livre2.getAuteur();
    if(_titre != livre2.getTitre())
        return _titre < livre2.getTitre();

    return false;
}

bool Livre::operator==(const Livre& livre2) const
{
    return _auteur == livre2.getAuteur() && _titre == livre2.getTitre()
                      && _annee == livre2._annee;
}

bool Livre::operator!=(const Livre& livre2) const
{
    return _auteur != livre2.getAuteur() || _titre != livre2.getTitre()
                      || _annee != livre2._annee;
}


ostream& operator<<(std::ostream& os, Livre& livre)
{
    os << livre.getTitre() << ";" << livre.getAuteur() << ";" << livre.getAnnee();
    return os;
}

istream& operator>>(std::istream& is, Livre& livre)
{
    string auteur, titre, annee;
    getline(is, auteur,';');
    getline(is, titre,';');
    getline(is, annee);
    livre=Livre(auteur, titre, stoi(annee));
    return is;
}

