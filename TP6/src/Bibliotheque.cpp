#include "Bibliotheque.hpp"
#include "Livre.hpp"
#include <algorithm>
#include <sstream>
#include <fstream>
using namespace std;
void Bibliotheque::afficher() const
{
    for(Livre livre : *this)
        cout<<livre;
}

void Bibliotheque::trierParAuteurEtTitre()
{
    sort(begin(), end());
}

void Bibliotheque::trierParAnnee()
{
    sort(begin(), end(), [](Livre livre1, Livre livre2)
        { return livre1.getAnnee() < livre2.getAnnee(); });
}

void Bibliotheque::ecrireFichier(const std::string& nomFichier) const
{
    ofstream os(nomFichier);
    if(os)
    {
        for(Livre livre : *this)
            os<<livre<<"\n";
    }

}

void Bibliotheque::lireFichier(const std::string& nomFichier)
{
    ifstream is(nomFichier);
    string auteur, titre, annee;
    if(!is)
        throw string("erreur : lecture du fichier impossible");
    while(getline(is, auteur, ';'))
    {
        getline(is, titre, ';');
        getline(is, annee);
        push_back(Livre(auteur, titre, stoi(annee)));
    }


}
