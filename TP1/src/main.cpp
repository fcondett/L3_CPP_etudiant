#include <iostream>
#include "fibonacci.hpp"
#include "Vecteur3.hpp"
int main()
{
    std::cout<<"recursif: "<<fibonacciRecursif(7)<<std::endl;
    std::cout<<"iteratif: "<<fibonacciIteratif(7)<<std::endl;

    Vecteur3D v= {2, 3, 6};
    afficher(v);
    std::cout<<norme(v)<<std::endl;

    std::cout<<produitScalaire(v, v)<<std::endl;
    afficher(addition(v,v));
    return 0;
}
