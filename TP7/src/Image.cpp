#include "Image.hpp"
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <iostream>
#include <cstring>

using namespace std;
Image::Image(int largeur, int hauteur): _largeur(largeur), _hauteur(hauteur)
{
    _pixels=new int[largeur*hauteur];
}

Image::~Image()
{
    delete []_pixels;
}

Image::Image(const Image &img)
{
    _largeur=img.getLargeur();
    _hauteur=img.getHauteur();
    _pixels=new int[_largeur * _hauteur];
    for(int i=0; i<_largeur; i++)
    {
        for(int j=0; j<_hauteur; j++)
        {
            setPixel(i,j,img.getPixel(i,j));
        }
    }
}

int Image::getLargeur() const
{
    return _largeur;
}

int Image::getHauteur() const
{
    return _hauteur;
}

const int& Image::getPixel(int i, int j) const
{
    if((i > _largeur || i < 0) || (j >_hauteur || j < 0))
    {
        throw string("erreur : coordonnés invalides");
    }
    int line=i * _hauteur;
    return _pixels[j + line];
}

void Image::setPixel(int i, int j, int couleur)
{
    if((i > _largeur || i < 0) || (j > _hauteur || j < 0))
    {
        throw string("erreur : coordonnés invalides");
    }
    int line=i * _hauteur;
    _pixels[j + line]=couleur;
}

const Image & Image::operator=(const Image & img)
{
    if(this != &img)
    {
        delete []_pixels;
        _largeur=img.getLargeur();
        _hauteur=img.getHauteur();
        _pixels=new int[_largeur * _hauteur];
        memcpy(_pixels, img._pixels, (_largeur*_hauteur)*sizeof(int));
        _pixels=img._pixels;
    }
    return *this;
}

void ecrirePnm(const Image & img, const std::string & nomFichier)
{
    ofstream ofs(nomFichier);
    if(ofs)
    {
        ofs<<"P2"<<endl;
        int largeur=img.getLargeur();
        int hauteur=img.getHauteur();
        ofs<<largeur<<" "<<hauteur<<endl;
        ofs<<255<<endl;
        int x=0;
        for(int i=0; i<largeur; i++)
        {
            for(int j=0; j<hauteur; j++)
            {
                ofs<<img.getPixel(i,j)<<endl;
            }
            x=x+hauteur;
        }
        ofs.close();
    }
}

void remplir(Image &img)
{
    int largeur=img.getLargeur();
    int hauteur=img.getHauteur();
    for(int i=0; i<largeur; i++)
    {
        for(int j=0; j<hauteur; j++)
        {
            float c=(1+cos((2*M_PI*j)*4 / (float) hauteur)) * 127;
            img.setPixel(i,j,c);
        }
    }
}

Image bordure(const Image & img, int couleur, int epaisseur)
{
    int largeur=img.getLargeur();
    int hauteur=img.getHauteur();
    Image im=img;
    for(int i=0; i<largeur; i++)
    {
        for(int j=0; j<epaisseur; j++)
        {
            im.setPixel(i,j,couleur);
        }
    }
    for(int i=0; i<epaisseur; i++)
    {
        for(int j=0; j<hauteur; j++)
        {
            im.setPixel(i,j,couleur);
        }
    }
    for(int i=0; i<largeur; i++)
    {
        for(int j=hauteur-epaisseur; j<hauteur; j++)
        {
            im.setPixel(i,j,couleur);
        }
    }
    for(int i=largeur-epaisseur; i<largeur; i++)
    {
        for(int j=0; j<hauteur; j++)
        {
            im.setPixel(i,j,couleur);
        }
    }
    return im;
}
