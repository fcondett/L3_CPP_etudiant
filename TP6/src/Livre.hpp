#ifndef LIVRE_HPP
#define LIVRE_HPP
#include <string>
#include <iostream>
class Livre {
private:
    std::string _titre;
    std::string _auteur;
    int _annee;

public:
    Livre();
    Livre(const std::string& titre, const std::string& auteur, int annee);
    const std::string& getTitre() const;
    const std::string& getAuteur() const;
    int getAnnee();
    bool operator<(const Livre& livre2) const;
    bool operator==(const Livre& livre2) const;
    bool operator!=(const Livre& livre2) const;

};
std::ostream& operator<<(std::ostream& os, Livre& livre);
std::istream& operator>>(std::istream& is, Livre& livre);
#endif

