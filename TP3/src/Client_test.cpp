#include "Client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_constr)  {
    Client c(42,"toto");
    CHECK(c.getId() == 42);
    CHECK(c.getNom() == "toto");
}
