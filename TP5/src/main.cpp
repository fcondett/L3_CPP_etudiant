#include "Ligne.hpp"
#include "Point.hpp"
#include "Couleur.hpp"
#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"
#include "ViewerFigures.hpp"
#include <iostream>
#include <vector>
#include <gtkmm.h>

int main(int argc, char** argv) {

    /*std::vector<FigureGeometrique*> figs {new Ligne(Couleur(125,125,125), Point(0,0), Point(100,100)), new PolygoneRegulier(Couleur(0,1,0), Point(100,200), 50, 5)};
    for (FigureGeometrique * ptrFig : figs)
            ptrFig->afficher();

    for (FigureGeometrique * ptrFig : figs)
            delete ptrFig;
        return 0;
    */
        
    /*Gtk::Main kit(argc, argv); // application gtkmm
    Gtk::Window window; // fenetre principale
    window.show_all();
    kit.run(window); // lance la boucle evenementielle return 0;
    */
    
    ViewerFigures viewer(argc, argv);
    viewer.run();

    return 0;
}

